
public class Supermarket {
	private String nameotdela;
	private String productCode;
	private String name;
	private String country;
    private	int retailPrice;
    private String namesource;
	
	public Supermarket(String nameotdela, String productCode, String name, String country, int retailPrice, String namesource)
	{
		this.nameotdela = nameotdela;
		this.productCode=productCode;
		this.name=name;
		this.country=country;
		this.retailPrice=retailPrice;
		this.namesource=namesource;	
	}
	public Supermarket(Supermarket sup)
	{
		this.nameotdela =sup.nameotdela;
		this.productCode=sup.productCode;
		this.name=sup.name;
		this.country=sup.country;
		this.retailPrice=sup.retailPrice;
		this.namesource=sup.namesource;	
	}
	public String getnameotdela()
	{
		return nameotdela;
	}
	public void setnameotdela(String nameotdela)
	{
		this.nameotdela=nameotdela;
	}
	public String getproductCode()
	{
		return productCode;
	}
	public void setproductCode(String productCode)
	{
		this.productCode=productCode;
	}public String getname()
	{
		return name;
	}
	public void setname(String name)
	{
		this.name=name;
	}public String getcountry()
	{
		return country;
	}
	public void setcountry(String country)
	{
		this.country=country;
	}public int getretailPrice()
	{
		return retailPrice;
	}
	public void setretailPrice(int retailPrice)
	{
		this.retailPrice=retailPrice;
	}
	public String getnamesource()
	{
		return namesource;
	}
	public void setnamesource(String namesource)
	{
		this.namesource=namesource;
	}
	public String toString()
	{
		return "�������� ������:"+nameotdela+"\n"+"��� ������:"+productCode+"\n"+"������������ ������:"+name+"\n"+"������-�������������:"+country+"\n"+"��������� ����:"+retailPrice+"\n"+"���������:"+namesource+"\n" ;
	}

   
}
